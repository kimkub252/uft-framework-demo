﻿
iRowCount = Datatable.getSheet("TC02 [TC02]").getRowCount
searchData = Trim((DataTable("SearchData","TC02 [TC02]")))

searchData = "ตัวอย่างหนังสือรับรองการทำงาน"

Call FW_TransactionStart("TC02 การไฟฟ้านครหลวง (กฟน.) OpenWeb")
Call FW_OpenWebBrowser("https://www.mea.or.th","IE")
Call FW_TransactionEnd("TC02 การไฟฟ้านครหลวง (กฟน.) OpenWeb")

Call FW_TransactionStart("TC02 การไฟฟ้านครหลวง (กฟน.) Click Link เข้าสู่เว็บไซต์")
'Browser("การไฟฟ้านครหลวง (กฟน.)").Page("การไฟฟ้านครหลวง (กฟน.)").Link("เข้าสู่เว็บไซต์").Click
Call FW_Link("TC02","การไฟฟ้านครหลวง (กฟน.)","การไฟฟ้านครหลวง (กฟน.)","เข้าสู่เว็บไซต์")
Call FW_TransactionEnd("TC02 การไฟฟ้านครหลวง (กฟน.) Click Link เข้าสู่เว็บไซต์")


Call FW_TransactionStart("TC02 การไฟฟ้านครหลวง (กฟน.) Click Image Search")
'Browser("การไฟฟ้านครหลวง (กฟน.)").Page("การไฟฟ้านครหลวง (กฟน.)_2").Image("search").Click
Call FW_Image("TC02","การไฟฟ้านครหลวง (กฟน.)","การไฟฟ้านครหลวง (กฟน.)_2","search")
Call FW_TransactionEnd("TC02 การไฟฟ้านครหลวง (กฟน.) Click Image Search")

Call FW_TransactionStart("TC02 การไฟฟ้านครหลวง (กฟน.) พิมพ์คำค้นหา") @@ hightlight id_;_Browser("การไฟฟ้านครหลวง (กฟน.)").Page("การไฟฟ้านครหลวง (กฟน.) 3").WebElement("ค้นหา")_;_script infofile_;_ZIP::ssf4.xml_;_
'Browser("การไฟฟ้านครหลวง (กฟน.)").Page("การไฟฟ้านครหลวง (กฟน.)_2").WebEdit("พิมพ์คำค้นหา").Set "ตัวอย่างหนังสือรับรองการทำงาน"
Call FW_WebEdit("TC02","การไฟฟ้านครหลวง (กฟน.)","การไฟฟ้านครหลวง (กฟน.)_2","พิมพ์คำค้นหา", searchData)
Call FW_TransactionEnd("TC02 การไฟฟ้านครหลวง (กฟน.) พิมพ์คำค้นหา")

Call FW_TransactionStart("TC02 การไฟฟ้านครหลวง (กฟน.) Click CheckBox ศูนย์ข้อมูลข่าว")
'Browser("การไฟฟ้านครหลวง (กฟน.)").Page("การไฟฟ้านครหลวง (กฟน.)_2").WebElement("ศูนย์ข้อมูลข่าว").Click
Call FW_WebElement("TC02","การไฟฟ้านครหลวง (กฟน.)","การไฟฟ้านครหลวง (กฟน.)_2","ศูนย์ข้อมูลข่าว")
Call FW_TransactionEnd("TC02 การไฟฟ้านครหลวง (กฟน.) Click CheckBox ศูนย์ข้อมูลข่าว")

Call FW_TransactionStart("TC02 การไฟฟ้านครหลวง (กฟน.) Click Radio รายการข้อมูล ใหม่ - เก่า")
'Browser("การไฟฟ้านครหลวง (กฟน.)").Page("การไฟฟ้านครหลวง (กฟน.)_2").WebElement("รายการข้อมูล ใหม่ - เก่า").Click @@ hightlight id_;_Browser("การไฟฟ้านครหลวง (กฟน.)").Page("การไฟฟ้านครหลวง (กฟน.) 3").WebEdit("end date")_;_script infofile_;_ZIP::ssf12.xml_;_
Call FW_WebElement("TC02","การไฟฟ้านครหลวง (กฟน.)","การไฟฟ้านครหลวง (กฟน.)_2","รายการข้อมูล ใหม่ - เก่า")
Call FW_TransactionEnd("TC02 การไฟฟ้านครหลวง (กฟน.) Click Radio รายการข้อมูล ใหม่ - เก่า")

Call FW_TransactionStart("TC02 การไฟฟ้านครหลวง (กฟน.) Click ค้นหา")
'Browser("การไฟฟ้านครหลวง (กฟน.)").Page("การไฟฟ้านครหลวง (กฟน.)_2").WebButton("ค้นหา").Click @@ hightlight id_;_Browser("การไฟฟ้านครหลวง (กฟน.)").Page("การไฟฟ้านครหลวง (กฟน.) 3").WebButton("ค้นหา")_;_script infofile_;_ZIP::ssf13.xml_;_
Call FW_WebButton("TC02","การไฟฟ้านครหลวง (กฟน.)","การไฟฟ้านครหลวง (กฟน.)_2","ค้นหา")
Call FW_TransactionEnd("TC02 การไฟฟ้านครหลวง (กฟน.) Click ค้นหา")

'Browser("การไฟฟ้านครหลวง (กฟน.)").WinObject("Notification").WinButton("No").Click
If Browser("การไฟฟ้านครหลวง (กฟน.)").WinObject("Notification").Exist(5) Then
	If Browser("การไฟฟ้านครหลวง (กฟน.)").WinObject("Notification").WinButton("No").Exist(5) Then
		Wait 1
		Browser("การไฟฟ้านครหลวง (กฟน.)").WinObject("Notification").WinButton("No").Click
	End If
End If

Call FW_TransactionStart("TC02 การไฟฟ้านครหลวง (กฟน.) Check Message")
'Browser("การไฟฟ้านครหลวง (กฟน.)").Page("การไฟฟ้านครหลวง (กฟน.)_2").Link("ตัวอย่างหนังสือรับรองการทำงาน").GetROProperty("text")
Call FW_ValidateLinkMessage("TC02","การไฟฟ้านครหลวง (กฟน.)","การไฟฟ้านครหลวง (กฟน.)_2","ตัวอย่างหนังสือรับรองการทำงาน","ตัวอย่างหนังสือรับรองการทำงาน")
Call FW_TransactionEnd("TC02 การไฟฟ้านครหลวง (กฟน.) Check Message")

Call FW_TransactionStart("TC02 การไฟฟ้านครหลวง (กฟน.) CloseWeb")
Call FW_CloseWebBrowser("IE")
Call FW_TransactionEnd("TC02 การไฟฟ้านครหลวง (กฟน.) CloseWeb")
