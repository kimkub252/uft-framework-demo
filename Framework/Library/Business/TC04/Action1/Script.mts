﻿iRowCount = Datatable.getSheet("[TC04] [TC04]").getRowCount
emailData = Trim((DataTable("Email","[TC04] [TC04]")))
passwordData = Trim((DataTable("Password","[TC04] [TC04]")))
addItem = Trim((DataTable("addItem","[TC04] [TC04]")))

'Login
Call FW_OpenWebBrowser("TC04 Demoexex OpenWeb","http://demoexex.esy.es/opencart/","IE")
Call FW_Link("TC04 My Account","TC04","Your Store","Your Store","My Account")
Call FW_Link("TC04 กดปุ่ม Login","TC04","Your Store","Your Store","Login")
Call FW_WebEdit("TC04 กรอก Email","TC04","Your Store","Account Login","email", emailData)
Call FW_WebEdit("TC04 กรอก Password","TC04","Your Store","Account Login","password", passwordData)
Call FW_WebButton("TC04 กดปุ่ม Login","TC04","Your Store","Account Login","Login") @@ hightlight id_;_Browser("Your Store").Page("Account Login").WebButton("Login")_;_script infofile_;_ZIP::ssf5.xml_;_
'Shop for products
Call FW_Link("TC04 เลือกเมนู Desktops","TC04","Your Store","Account Login","Desktops")
Call FW_Link("TC04 เลือก Mac","TC04","Your Store","Account Login","Mac (1)")
Call FW_Image("TC04 คลิกรูป Mac","TC04","Your Store","Account Login","iMac")
Call FW_WebEdit("TC04 เลือก Item","TC04","Your Store","Account Login","quantity", addItem)
Call FW_WebButton("TC04 กดปุ่ม Add to Cart","TC04","Your Store","Account Login","Add to Cart")
Call FW_WebButton("TC04 กดปุ่ม item","TC04","Your Store","Account Login","10 item(s) - $1,000.00")
Call FW_Link("TC04 กดปุ่ม View","TC04","Your Store","Account Login","View Cart")
'Checkout
Call FW_WebButton("TC04 กดปุ่ม Checkout","TC04","Your Store","Account Login","Checkout")
Call FW_WebButton("TC04 กดปุ่ม Continue","TC04","Your Store","Account Login","Continue")
Call FW_WebButton("TC04 กดปุ่ม Continue_2","TC04","Your Store","Account Login","Continue_2")
Call FW_WebButton("TC04 กดปุ่ม Continue_3","TC04","Your Store","Account Login","Continue_3")
Call FW_WebCheckBoxON("TC04 กดเลือก agree NO","TC04","Your Store","Account Login","agree")
Call FW_WebButton("TC04 กดปุ่ม Continue_4","TC04","Your Store","Account Login","Continue_4")
Call FW_WebButton("TC04 กดปุ่ม Confirm Order","TC04","Your Store","Account Login","Confirm Order") @@ hightlight id_;_Browser("Your Store").Page("Account Login").WebButton("Confirm Order")_;_script infofile_;_ZIP::ssf20.xml_;_
Call FW_ValidateElementMessage("TC04 ตรวจสอบข้อความ Your order has been placed!","TC04","Your Store","Account Login","Your order has been placed!", "Your order has been placed!")
Call FW_WebButton("TC04 กดปุ่ม Continue_5","TC04","Your Store","Account Login","Continue_5")
'Logout
Call FW_Link("TC04 กดปุ่ม My Account","TC04","Your Store","Your Store","My Account")
Call FW_Link("TC04 กดปุ่ม Login","TC04","Your Store","Your Store","Logout")
Call FW_CloseWebBrowserIE("TC04 Demoexex CloseWeb")
